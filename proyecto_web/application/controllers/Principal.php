<?php  

/**
* 
*/
class Principal extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

    //Para Cargar el dashboard y los rides 
	 public function load_dashboard()
    {
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $data['user_name'] = $_SESSION['user_name'];
        $data['img'] = $_SESSION['img'];
        $data['ride'] = $this->Ride_model->CargarRideUser($_SESSION['id_user']);
        $this->load->view('principal/dashboard',$data);
        } else {
            $this->session->set_flashdata('error','Primero se debe logear');
            redirect('User/load_login');
            
        }
    }

    //Para cargar la pagina de inicio
	 public function load_inicio()
    {
        
        $this->load->view('principal/inicio');
    }

    //Para buscar los rides en el inicio
    public function BuscarRide()
    {
            $start = $_POST['start'];
            $end = $_POST['end'];
            $data['ride'] = $this->Principal_model->BuscarRides($start,$end);
            $this->load->view('principal/inicio',$data);
    }
}
?>