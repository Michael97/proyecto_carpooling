<?php  

/**
* 
*/
class User extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

    //Para cargar en login del proyecto
	 public function load_login()
    {
        
        $data['login_error'] = $this->session->flashdata('login_error');
        $data['error'] = $this->session->flashdata('error');
        $this->load->view('users/login',$data);
    }

    //Para cargar la pagina del registro
	public function load_registro()
    {
        $data['msj'] = $this->session->flashdata('msj');
        $this->load->view('users/registros',$data);
    }

    //Para cargar la pagian de settings de usuario y mostrar datos 
	 public function load_settings()
    {
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $data['user_name'] = $_SESSION['user_name'];
          $data['img'] = $_SESSION['img'];
        $data['dato'] = $this->User_model->CargarSetting($_SESSION['id_user']);
        $this->load->view('users/settings',$data);
        } else {
             $this->session->set_flashdata('error','Primero se debe logear');
            redirect('User/load_login');
            
            
        }
      
    }
    //Para obtener datos del usuario a registrar y mandarlos al modelo
    public function RegistrarUser()
    {

        $config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", 
            'max_height' => "2000",
            'max_width' => "2000"
        );

          $rules = array(
            "password" => array(
                "field" => "password",
                "label" => "pass_conf",
                "rules" => "required"
              ),
            "r_pass" => array(
                "field" => "rpassword",
                "label" => "pass_conf",
                "rules" => "required|matches[password]"
              )
          );
         $this->load->library('upload', $config);
        if($this->upload->do_upload()){
            $data = array('upload_data' => $this->upload->data());
            $file_data = $this->upload->data(); 


            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()) {
                $data = $array = array(
                    'name' => $_POST['first_name'], 
                    'last_name' => $_POST['last_name'],
                    'phone' => $_POST['phone'],
                    'user_name' => $_POST['user'],
                    'password' => $_POST['password'],
                    'img' => $file_data['file_name']);
         
           $this->User_model->Registrar($data);
                        redirect('User/load_login');
        
            } else {
                    $this->session->set_flashdata('msj','Las contraseñas no coinciden');
                    redirect('User/load_registro');
            }
        }
    }

    //Para autentificar a un usuario
    public function Autentificar()
    {
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];

      
        $result = $this->User_model->Login($user_name,$password);
    
        if (sizeof($result) > 0) {
            foreach ($result as $results) {
                $_SESSION['loggedin'] = true;
                $_SESSION['id_user'] = $results['id_user'];
                $_SESSION['user_name'] = $results['user_name'];
                $_SESSION['img'] = $results['img'];
            }
            redirect('Principal/load_dashboard');
        } else {
           $this->session->set_flashdata('login_error','Error de Usuario o Contraseña');
            redirect('User/load_login');
        }

    }

    //Obtener datos para settings del usuario
    public function GuardarSetting()
    {   
         $data = $array = array(
            'full_name' => $_POST['full_name'],
            'speed_average' => $_POST['speed_average'],
            'information' => $_POST['information'],
            'id_user' => $_SESSION['id_user'] 
            );

        if (isset($_POST['guardar_setting'])) {
            $this->User_model->GuardarSetting($data);
            redirect('Principal/load_dashboard');
        } elseif (isset($_POST['editar_setting'])) {
             $this->User_model->EditarSetting($_SESSION['id_user'],$data);
            redirect('Principal/load_dashboard');
        }
    }

    //Para hacer log out del proyecto
    public function Logout()
    {
        session_destroy();
        redirect('Principal/load_inicio');
    }
}
?>