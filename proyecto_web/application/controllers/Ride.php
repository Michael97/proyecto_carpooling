<?php  

/**
* 
*/
class Ride extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

    //Para cargar la pagina de agregar rides
	 public function load_ride()
    {
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $data['user_name'] = $_SESSION['user_name'];
         $data['img'] = $_SESSION['img'];
         $data['dias_error'] = $this->session->flashdata('dias_error');
         $this->load->view('ride/rides',$data);
        } else {
             $this->session->set_flashdata('error','Primero se debe logear');
            redirect('User/load_login');
            
            
        }
    }

    //Para cargar la pagina de editar rides 
	 public function load_edit()
    {
     
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $data['user_name'] = $_SESSION['user_name'];
        $data['img'] = $_SESSION['img'];
        $data['ride'] = $this->Ride_model->CargarRideEdit($_POST['edit']);
       // $data['dia'] = $this->Ride_model->CargarDiasRide($_POST['edit']);
        $data['dias_error'] = $this->session->flashdata('dias_error');
        $this->load->view('ride/rides_edit',$data);
    } else {
        $this->session->set_flashdata('error','Primero se debe logear');
        redirect('User/load_login');    
       }
        
    }

    //Para cargar la pagina donde se muestra la informacion de un ride buscado
	 public function load_ride_view()
    {
        $result = $this->Ride_model->MostrarDatosRide($_POST['view']);
        if (sizeof($result) >0) {
            $data['dato'] = $result;
        } else {
            $data['ride'] =  $this->Ride_model->CargarRideEdit($_POST['view']);
        }
        $this->load->view('ride/view_rides',$data);
        
    }

    //Para obtener datos de un ride a agregar y pasarlos al modelo
    public function AgregarRide()
    {
        
        if (isset($_POST['dia'])) {

            $dias = $_POST['dia'];

            $totalDias = "";
            for($i=0; $i < count($dias); $i++){
                $totalDias = $totalDias.$dias[$i].",";  
            }
            $data = $array = array(
                'ride_name' => $_POST['rideName'],
                'start' => $_POST['start'],
                'end' => $_POST['end'],
                'description' => $_POST['description'],
                'departure' => $_POST['departure'],
                'arrival' => $_POST['arrival'],
                'dia' => $totalDias,
                'id_user' => $_SESSION['id_user']
             );

            if (isset($_POST['guardar_ride'])) {
                $this->Ride_model->AgregarRide($data);
                redirect('Principal/load_dashboard');
            
            } elseif (isset($_POST['edit_ride'])) {
                $this->Ride_model->EditarRide($_POST['edit_ride'],$data);
                redirect('Principal/load_dashboard');                
            }

        } elseif (isset($_POST['url_add'])) {
            $this->session->set_flashdata('dias_error','Select at least one day');
            $url = $_POST['url_add'];
            header("Location: $url");
        } 
    }

    //Para obtener los datos de un ride por eliminar
    public function EliminarRide()
    {       
       $this->Ride_model->EliminarRide($_POST['delete']);
        redirect('Principal/load_dashboard');
    }
}
?>