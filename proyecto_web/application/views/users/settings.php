<!DOCTYPE html>
<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" 
		  		class="img-responsive" alt="Responsive image">
		  	</div>
		 	<div class="col-md-4"></div>
		</div>
	</div>
	<br>

	<div class="row">
  		<div class="col-md-3 col-md-offset-2">
			<ul class="nav nav-tabs">
			 	<li role="presentation"><a href="<?php echo base_url().'Principal/load_dashboard'?>" target="_self">Dashboard</a></li>
			  	<li role="presentation"><a href="<?php echo base_url().'Ride/load_ride'?>" target="_self">Rides</a></li>
			  	<li role="presentation" class="active"><a href="<?php echo base_url().'User/load_settings'?>" target="_self">Settings</a></li>
			</ul>
		</div>
	  	<div class="col-md-3 col-md-offset-3">
	  		<ul class="nav nav-tabs">
	  			<li role="presentation" class="dropdown">
				 	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
				    	<label class="welcome">Welcome <?php echo $user_name;?></label>
				    	<img class='img_user' src='<?php echo base_url().'/uploads/'.$img?>'> 
				   		<span class="caret"></span>
				  	</a>
				    <ul class="dropdown-menu">
						<a href="<?php echo base_url();?>User/Logout">Logout</a>
				    </ul>
	  			</li>
	  		</ul>
		 </div>
	</div>

	<div>
	<div class="container">
		<label class="tlabel">Settings</label>
	</div>
	<div class="container">
		<ul>
			<li><a href="<?php echo base_url().'Principal/load_dashboard'?>" target="_self">Dashboard ></a></li>
			<li>Settings</li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
 			<div class="col-md-4 col-md-offset-3 col-md-offset-4">
 				<?php if (empty($dato)):?>
 				<form method="post" action="<?php echo base_url().'User/GuardarSetting'?>">
					<p><label for="FullName">Full Name</label></p>
					<p><input name="full_name" type="text" placeholder="FullName" class="form-control"></input></p>
					<p><label for="text">Speed Average</label></p>
					<p><input name="speed_average" type="text" placeholder="Speed Average" class="form-control"></input></p>
					<p><label>About me</label></p>
					<p><textarea name="information" class="txt">Somenthing About Me Goes Here</textarea></p>
					<div class="row">
						 <div class="col-md-3"><a href="<?php echo base_url().'Principal/load_dashboard'?> " target="_self">Cancel</a></div>
						 <div class="col-md-4 col-md-offset-4"> 
						 	<input name="guardar_setting" type="submit" class="btn btn-success" name="boton" value="Save"/>
						 </div>
					</div>
					<br>
				</form>
				<?php else:?>
					<form method="post" action="<?php echo base_url().'User/GuardarSetting'?>">
	 					<?php foreach ($dato as $datos):?>
						<p><label for="FullName">Full Name</label></p>
						<p><input name="full_name" type="text" placeholder="FullName" class="form-control" 
							value="<?php echo $datos['full_name'];?> "></input></p>
						<p><label for="text">Speed Average</label></p>
						<p><input name="speed_average" type="text" placeholder="Speed Average" class="form-control"
							value="<?php echo $datos['speed_average'];?> "></input></p>
						<p><label>About me</label></p>
						<p><textarea name="information" class="txt"><?php echo isset($datos)?$datos['information']:"Somenthing About Me Goes Here";?></textarea></p>
						<?php endforeach;?>
						<div class="row">
							 <div class="col-md-3"><a href="<?php echo base_url().'Principal/load_dashboard'?> " target="_self">Cancel</a></div>
							 <div class="col-md-4 col-md-offset-4"> 
							 	<p><a href="<?php echo base_url().'User/EditarSetting';?>" target="_self"><input name="editar_setting" type="submit" class="btn btn-success" name="boton" value="Editar"/></a></p>	
							 </div>
						</div>
						<br>
					</form>
				<?php endif;?>
 			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</body>
</html>