<!DOCTYPE html>
<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	<div class="container">
		<form class="form-signin" method="post" action="<?php echo base_url().'User/RegistrarUser'?>" enctype="multipart/form-data">
			<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" class="img-responsive" alt="Responsive image">
			<br>
			<label for="fileToUpload">
				<img id ='img' class="img-responsive img_perfil"  src="https://image.freepik.com/free-icon/add-people-interface-symbol-of-black-person-close-up-with-plus-sign-in-small-circle_318-56589.jpg">
			</label>
			<p><label>Favor seleccione una imagen para su perfil</label></p>
	   		<input class="input_img" type="file" name="userfile" id="fileToUpload" 
	    	onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
			<p><label for="FirtsName">First Name</label></p>
			<p><input type="text" class="form-control" id="first_name" name="first_name" placeholder="Input Text" required></p>
			<p><label for="LastName">Last Name</label></p>
			<p><input type="text" class="form-control" id="last_name" name="last_name" placeholder="Input Text" required></p>
			<p><label for="Phone">Phone</label></p>
			<p><input type="tel"  class="form-control" id="phone" name="phone" placeholder="(XXX)XXXXXXXX" required></p>
			<p><label for="User">User Name</label></p>
			<p><input type="text" class="form-control" id="user" name="user" placeholder="Input Text" required></p>
			<p><label for="Password">Password</label></p>
			<p><input type="password" class="form-control" id="password" name="password" placeholder="Input Text" required></p>
			<p><label>Repeat Password</label></p>
			<p class="error"><?php echo isset($msj)?$msj:'';?></p>
			<p><input type="password" class="form-control" name="rpassword" placeholder="Input text" required></p>
			<p><label>Already an User </label><a href="<?php echo base_url().'User/load_login'?>"> Login Here</a></p>
			<p><a target="_self"> <input type="submit" class="btn btn-lg btn-info btn-block" name="boton" value="Register"/></a></p>
		</form> 
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

</body>
</html>