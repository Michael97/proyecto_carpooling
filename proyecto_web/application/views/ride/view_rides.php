<!DOCTYPE html>
<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	
	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" 
		  		class="img-responsive" alt="Responsive image">
		  	</div>
		 	<div class="col-md-4"></div>
		</div>
	</div>
	<br>

	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<div class="panel panel-default">
			 		<div class="panel-heading">Ride Data:</div>
			  		<div class="panel-body">
			  			<?php if (isset($dato)): ?>
			  				<?php foreach ($dato as $datos):?>
								<p><label>Ride Name: <?php echo $datos['ride_name'];?></label></p>
								<p><label>Start: <?php echo $datos['start'];?></label></p>
								<p><label>End: <?php echo $datos['end'];?></label></p>
								<p><label>Description: <?php echo $datos['description'];?></label></p>
								<p><label>Departure (24h format): <?php echo $datos['departure'];?></label></p>
								<p><label>Arrival (24h format): <?php echo $datos['arrival'];?></label></p>
			  					<p><label>Days: <?php echo $datos['dia'];?></label></p>
			  					<p><label>Creator Data</label></p>
			  					<p><label>Name: <?php echo $datos['full_name'];?></label></p>
			  					<p><label>Speed Average: <?php echo $datos['speed_average'];?></label></p>
			  					<p><label>Some Information: <?php echo $datos['information'];?></label></p>
			  				<?php endforeach;?>
			  			<?php else:?>
			  				<?php foreach ($ride as $rides):?>
								<p><label>Ride Name: <?php echo $rides['ride_name'];?></label></p>
								<p><label>Start: <?php echo $rides['start'];?></label></p>
								<p><label>End: <?php echo $rides['end'];?></label></p>
								<p><label>Description: <?php echo $rides['description'];?></label></p>
								<p><label>Departure (24h format): <?php echo $rides['departure'];?></label></p>
								<p><label>Arrival (24h format): <?php echo $rides['arrival'];?></label></p>
			  					<p><label>Days: <?php echo $rides['dia'];?></label></p>
			  					<p><label>Creator Data</label></p>
			  					<p><label>No se encontraron datos para este usuario</label></p>
			  				<?php endforeach;?>
			  			<?php endif;?>
			 		</div>
				</div>
		  	</div>
		  	<div class="col-md-4"></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<a href="<?php echo base_url().'Principal/load_inicio'; ?>" target="_self"><button type="button" class="btn btn-primary">Volver</button></a>
		  	</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</body>
</html>