<!DOCTYPE html>
<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  	<script src="<?php echo base_url();?>assets/js/script.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	
	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" 
		  		class="img-responsive" alt="Responsive image">
		  	</div>
		 	<div class="col-md-4"></div>
		</div>
	</div>
	<br>	


	<br>
	<div class="row">
  		<div class="col-md-3 col-md-offset-2">
			<ul class="nav nav-tabs">
			 	<li role="presentation" class="active"><a href="<?php echo base_url().'Principal/load_dashboard'?>" 
			 	target="_self">Dashboard</a></li>
			  	<li role="presentation"><a href="<?php echo base_url().'Ride/load_ride'?> " target="_self">Rides</a></li>
				<li role="presentation"><a href="<?php echo base_url().'User/load_settings'?>" target="_self">Settings</a></li>
			</ul>
		</div>
	  	<div class="col-md-3 col-md-offset-3">
	  		<ul class="nav nav-tabs">
	  			<li role="presentation" class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
				    	<label class="welcome">Welcome <?php echo $user_name;?></label><img class='img_user' src='<?php echo base_url().'/uploads/'.$img?>'>
				   		<span class="caret"></span>
				  	</a>
				    <ul class="dropdown-menu">
						<a href="<?php echo base_url();?>User/Logout">Logout</a>
				    </ul>
	  			</li>
	  		</ul>
		 </div>
	</div>
	

	<div class="container">
		<div class="row">
  			<div class="col-md-3 .col-md-offset-3">
  				<label>Dashboard</label>
  				<ul>
					<li><a href="<?php echo base_url().'Principal/load_dashboard'?>" target="_self">Dashboard ></a></li>
				</ul>
				<label>My Rides</label>
  			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
		  	<div class="col-md-1"></div>
		  	<div class="col-md-9">
		  		<div class="panel panel-default panel-prin">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-4">your current list of rides</div>
							<div class="col-md-7"></div>
					 		<div class="col-md-1">
								<a href="<?php echo base_url().'Ride/load_ride'?>" target="_self">
								 	<button  type="button" class="btn btn-primary">+</button></a>
							</div>
						</div>
					</div>
					<div class="panel-body">
			    		<div class="table-responsive">
			    			<?php if (empty($ride)):?>
							<table name="results" class="table table-bordered table-hover">
								<tr>
									<th>Name</th>
									<th>Start</th>
									<th>End</th>
									<th>Actions</th>
								</tr>
									
							</table>
							<?php else:?>
								<table name="results" class="table table-bordered table-hover">
									<tr>
										<th>Name</th>
										<th>Start</th>
										<th>End</th>
										<th>Actions</th>
									</tr>
								<?php foreach ($ride as $rides):?>
									<tr>
										<td><?php echo $rides['ride_name']; ?></td>
										<td><?php echo $rides['start']; ?></td>
										<td><?php echo $rides['end']; ?></td>
										<td>	
										 	<form class="form_edit" method="POST" 
										 		action="<?php echo base_url().'Ride/load_edit'?>">
												<button class="btn btn-warning" type = "submit" name="edit" 
												value="<?php echo $rides['id_ride'];?>" >Edit
										         </button>
										         <span>--</span>
											</form>
											<form name="form_eliminar" method="POST" action="<?php echo base_url().'Ride/EliminarRide'?>">
												<button onclick="if(confirm('Desea Eliminar este Ride?')){
													this.form.submit();}" class="btn btn-danger" type = "button">delete
										         </button>
										         <input type="hidden" name="delete" value="<?php echo $rides['id_ride'];?>">
											</form>
										</td>
									</tr>
								<?php  endforeach;?>	
								</table>
							<?php endif; ?>
						</div>
		 			</div>
		 			<div class="panel-footer">
		 				<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-7"></div>
					 		<div class="col-md-1">
								<a href="<?php echo base_url().'Ride/load_ride'?>" target="_self">
								 	<button  type="button" class="btn btn-primary">+</button>
								</a>
							</div>
						</div>
		 			</div>
				</div>
		  	</div>
		  	<div class="col-md-4"></div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/script.js"></script>
</body>
</html>