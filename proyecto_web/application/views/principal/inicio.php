	<!DOCTYPE html>
<html>
<head>
	<title>TicoRides</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">
	
	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<img src="http://images.financialexpress.com/2014/12/carpooling.jpg" 
		  		class="img-responsive" alt="Responsive image">
		  		<label>Welcome to TicoRides.com</label>
		  	</div>
		 	<div class="col-md-4"></div>
		</div>
	</div>
	<br>

	<div class="container">
		<div class="row">
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4"></div>
		  	<div class="col-md-4">
		  		<div class="btn-group" role="group"">
			  		<a href="<?php echo base_url().'User/load_registro'?>" target="_self">
			  			<button type="button" class="btn btn-primary">Sign in</button></a>
			  		<a href="<?php echo base_url().'User/load_login'?>" target="_self">
			  		<button type="button" class="btn btn-info">Login</button></a>
				</div>
		  	</div>
		</div>
	</div>	
	<br>

	<div class="row">
  		<div class="col-md-3"></div>
  		<div class="col-md-6">
  			<div class="panel panel-default">
		  		<div class="panel-heading">Search for a ride</div>
		  			<div class="panel-body">
		    			<form class="form-inline" method="post" action="<?php echo base_url().'Principal/BuscarRide'?>">
							<label>From</labe>
							<input id="start" name="start" type="text" class="form-control" placeholder="Location" required>
							<label>To</label>
							<input id="end" name="end" type="text" class="form-control" placeholder="Input Text" required>  
								<input name="buscar_ride" type="submit" class="btn btn-primary"  
								value="Find my Ride"/>
							
						</form>
		  			</div>
				</div>
			</div>
  		</div>
  		<div class="col-md-4"></div>
	</div>
	<br>
	<div class="container">
		<div class="row">
		 	<div class="col-md-2"></div>
		  	<div class="col-md-8">
		  		<div class="panel panel-default">
			 		<div class="panel-heading">Results for Rides that matches your criteria: </div>
			  		<div class="panel-body">
			    		<div class="table-responsive">
			    		<?php if (empty($ride)):?>
							<table name="results" class="table table-bordered table-hover table-striped">
								<tr>
									<th>User</th>
									<th>Start</th>
									<th>End</th>
									<th></th>
								</tr>	
							</table>
							<?php else: ?>
								<table name="results" class="table table-bordered table-hover table-striped">
									<tr>
										<th>User</th>
										<th>Start</th>
										<th>End</th>
										<th></th>
									</tr>
								<?php foreach ($ride as $rides):?>
									<tr>
										<td><?php echo $rides['ride_name']; ?></td>
										<td><?php echo $rides['start']; ?></td>
										<td><?php echo $rides['end']; ?></td>
										<td>
											<form method="POST" action="<?php echo base_url().'Ride/load_ride_view'?>">
												<button class="btn btn-danger" type = "submit" name="view" 
													value="<?php echo $rides['id_ride'];?>">view
										         </button>
											</form>
										</td>
									</tr>
								<?php  endforeach;?>	
								</table>
							<?php endif; ?>
						</div>
		 			</div>
		 		</div>
		  	</div>
		  	<div class="col-md-4"></div>
		</div>
	</div>


	<div id="map"></div>

	<script src = "http://code.jquery.com/jquery-1.10.1.min.js" type="text/javascript"></script>
 	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
 	<script src="<?php echo base_url();?>assets/js/map.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpigFKAB0g04kGberX6Lr22a0BRhMfsHI&libraries=places&callback=initMap"></script>
</body>
</html