<?php  
/**
* 
*/
class User_model extends Ci_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	//Registrar Usuario
	public function Registrar($data)
	{
		$this->db->insert('user',$data);
	}

	//Login Usaurio
	public function Login($user_name,$password)
	{
		$query = $this->db->get_where('user',
      	array('user_name' => $user_name, 'password' => $password));

	  	return $query->result_array();		
	}

	//Guarda los settings de un usuario
	public function GuardarSetting($data)
	{
		$this->db->insert('setting',$data);
	}

	//Carga los settings de un usuario
	public function CargarSetting($id)
	{
		$query = $this->db->get_where('setting',
			array('id_user' => $id));
		return $query->result_array();
	}

	//para editar los settings de un usuario
	public function EditarSetting($id,$datos)
	{
		$this->db->where('id_user', $id);
		$this->db->update('setting', $datos);
	}

}
?>