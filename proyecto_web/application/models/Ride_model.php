<?php  
/**
* 
*/
class Ride_model extends Ci_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	//Cargar rides del usuario
	public function CargarRideUser($id_user)
	{
		$query = $this->db->get_where('ride',
     	array('id_user' => $id_user));
	  	return $query->result_array();
	}

	//Agregar rides
	function AgregarRide($data)
	{
		$this->db->insert('ride',$data);
	}

	//Editar rides
	public function EditarRide($id,$datos)
	{
		$this->db->where('id_ride', $id);
		$this->db->update('ride', $datos);
	}

	//Eliminar rides
	public function EliminarRide($id_ride)
	{
		$this->db->where('id_ride', $id_ride);
		$this->db->delete('ride');	
	}

	//Carga los datos del ride a editar
	public function CargarRideEdit($id_ride)
	{
		$query = $this->db->get_where('ride',
     	array('id_ride' => $id_ride));
	  	return $query->result_array();
	}

	//Carga los datos del ride buscado por un usuario
	public function MostrarDatosRide($id_ride)
	{	
		$query = $this->db->query("SELECT ride.ride_name,ride.start,ride.end,ride.description,ride.departure,ride.arrival,ride.dia, setting.full_name, setting.speed_average, setting.information FROM ride INNER JOIN setting on ride.id_user = setting.id_user WHERE ride.id_ride = $id_ride");
		
		return $query->result_array();
	}
}
?>